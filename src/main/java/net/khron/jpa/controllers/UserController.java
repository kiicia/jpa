package net.khron.jpa.controllers;

import java.util.List;

import net.khron.jpa.entities.User;
import net.khron.jpa.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public User find(@PathVariable Long id) {
		return userService.find(id);
	}

	@RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
	@ResponseBody
	public List<User> findByName(@PathVariable String name) {
		return userService.find(name);
	}

	@RequestMapping(value = "/namehql/{name}", method = RequestMethod.GET)
	@ResponseBody
	public List<User> findByNameHql(@PathVariable String name) {
		return userService.findHql(name);
	}

	@RequestMapping(value = "/namesql/{name}", method = RequestMethod.GET)
	@ResponseBody
	public List<User> findByNameSql(@PathVariable String name) {
		return userService.findSql(name);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseBody
	public Long persist(@RequestBody User user) {
		return userService.persist(user);
	}

}
