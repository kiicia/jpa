package net.khron.jpa;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

/**
 * https://spring.io/guides/gs/accessing-data-jpa/
 *
 * http://docs.spring.io/spring/docs/current/spring-framework-reference/html/orm.html
 *
 * http://docs.jboss.org/hibernate/orm/4.3/topical/html/metamodelgen/MetamodelGenerator.html
 *
 * http://wiki.fasterxml.com/JacksonFeatureObjectIdentity
 *
 * dodatkowe detale w klasie org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
 *
 */
@SpringBootApplication
@EnableTransactionManagement
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	/**
	 * Custom data source
	 */
	@Bean
	public DataSource dataSource() {
		HikariDataSource dataSource = new HikariDataSource();
		// dataSource.setDriverClassName(com.mysql.jdbc.Driver.class.getCanonicalName());
		// dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8");
		// dataSource.setUsername("root");
		// dataSource.setPassword("12345");
		dataSource.setDriverClassName(org.h2.Driver.class.getCanonicalName());
		dataSource.setJdbcUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
		dataSource.setUsername("sa");
		return dataSource;
	}

	//
	// // tak można stworzyć wymagane beany na piechotę żeby móc w dao użyć
	// // @Autowired SessionFactory sessionFactory;
	//
	// @Bean
	// public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
	// LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	// sessionFactory.setDataSource(dataSource);
	// return sessionFactory;
	// }
	//
	// @Bean
	// public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) {
	// return new HibernateTransactionManager(sessionFactory);
	// }

	@Bean
	public PlatformTransactionManager transactionManager() {
		return new JpaTransactionManager();
	}

}
