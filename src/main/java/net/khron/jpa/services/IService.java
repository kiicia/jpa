package net.khron.jpa.services;

import net.khron.jpa.entities.MyEntity;

public interface IService<T extends MyEntity> {

	T find(Long id);

	Long persist(T ebtity);

}
