package net.khron.jpa.services;

import net.khron.jpa.daos.IDao;
import net.khron.jpa.entities.MyEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractService<T extends MyEntity> implements IService<T> {

	@Autowired
	protected IDao<T> dao;

	@Override
	@Transactional(readOnly = true)
	public T find(Long id) {
		return dao.find(id);
	}

	@Override
	@Transactional
	public Long persist(T user) {
		return dao.persist(user);
	}

}
