package net.khron.jpa.services;

import java.util.List;

import net.khron.jpa.daos.UserDao;
import net.khron.jpa.entities.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class UserServiceImpl extends AbstractService<User> implements UserService {

	@Autowired
	protected UserDao dao;

	@Override
	@Transactional(readOnly = true)
	public List<User> find(String name) {
		return dao.find(name);
	}

	@Override
	@Transactional(readOnly = true)
	public List<User> findHql(String name) {
		return dao.findHql(name);
	}

	@Override
	public List<User> findSql(String name) {
		return dao.findSql(name);
	}

}
