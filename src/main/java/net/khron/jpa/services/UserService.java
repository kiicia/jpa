package net.khron.jpa.services;

import java.util.List;

import net.khron.jpa.entities.User;

public interface UserService extends IService<User> {

	List<User> find(String name);

	List<User> findHql(String name);

	List<User> findSql(String name);

}
