package net.khron.jpa.daos;

import net.khron.jpa.entities.MyEntity;

public interface IDao<T extends MyEntity> {

	T find(Long id);

	Long persist(T user);

}
