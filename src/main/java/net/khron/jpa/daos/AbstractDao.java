package net.khron.jpa.daos;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.khron.jpa.entities.MyEntity;

public abstract class AbstractDao<T extends MyEntity> implements IDao<T> {

	@PersistenceContext
	protected EntityManager entityManager;

	// // jeśli chcieć używać sessionFactory wprost
	// @Autowired
	// protected SessionFactory sessionFactory;

	@Override
	public T find(Long id) {
		return entityManager.find(entityClass(), id);
	}

	@Override
	public Long persist(T entity) {
		entityManager.persist(entity);
		return entity.getId();
	}

	@SuppressWarnings("unchecked")
	protected Class<T> entityClass() {
		return (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	};

}
