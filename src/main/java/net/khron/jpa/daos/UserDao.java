package net.khron.jpa.daos;

import java.util.List;

import net.khron.jpa.entities.User;

public interface UserDao extends IDao<User> {

	List<User> find(String name);

	List<User> findHql(String name);

	List<User> findSql(String name);

}
