package net.khron.jpa.daos;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import net.khron.jpa.entities.User;
import net.khron.jpa.entities.User_;

import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl extends AbstractDao<User> implements UserDao {

	@Override
	public List<User> find(String name) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
		Root<User> root = cq.from(User.class);
		root.join(User_.addresses, JoinType.LEFT);
		cq.select(root);
		cq.where(cb.like(root.get(User_.name), "%" + name + "%"));
		return entityManager.createQuery(cq).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findHql(String name) {
		return entityManager.unwrap(Session.class)
				.createQuery("from User user left join fetch user.addresses address where user.name like :name")
				.setParameter("name", "%" + name + "%")
				.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findSql(String name) {
		return entityManager.unwrap(Session.class)
				.createSQLQuery("select * from user where user.name like :name")
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("name", "%" + name + "%")
				.list();
	}

}
