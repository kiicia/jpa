package net.khron.jpa.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

@Entity
public class Product extends MyEntity {

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "products")
	private List<User> users;

	private String uid;

	public String getUid() {
		return uid;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "Product: " + uid + " " + users.size();
	}

}
