package net.khron.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class UserData extends MyEntity {

	@OneToOne(mappedBy = "userData")
	private User user;

	private String phone;

	public String getPhone() {
		return phone;
	}

	public User getUser() {
		return user;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "UserData: " + phone + " " + (user == null ? null : user.getId());
	}

}
