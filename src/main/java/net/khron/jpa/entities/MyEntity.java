package net.khron.jpa.entities;

import java.lang.reflect.Field;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import net.vidageek.mirror.dsl.Mirror;

@MappedSuperclass
public class MyEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MyEntity other = (MyEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@PrePersist
	@PreUpdate
	public void reversedRelation() {
		for (Field field : this.getClass().getDeclaredFields()) {
			OneToMany oneToMany;
			OneToOne oneToOne;
			if (field.isAnnotationPresent(OneToMany.class) && (oneToMany = field.getAnnotation(OneToMany.class)).mappedBy() != null && !oneToMany.mappedBy().isEmpty()) {
				Iterable<?> i = (Iterable<?>) new Mirror().on(this).get().field(field);
				if (i != null) {
					for (Object o : i) {
						new Mirror().on(o).set().field(oneToMany.mappedBy()).withValue(this);
					}
				}
			} else if (field.isAnnotationPresent(OneToOne.class) && (oneToOne = field.getAnnotation(OneToOne.class)).mappedBy() != null && !oneToOne.mappedBy().isEmpty()) {
				Object o = new Mirror().on(this).get().field(field);
				if (o != null) {
					new Mirror().on(o).set().field(oneToOne.mappedBy()).withValue(this);
				}
			}

		}
	}

	public void setId(Long id) {
		this.id = id;
	}

}
