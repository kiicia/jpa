package net.khron.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Address extends MyEntity {

	private String city;

	@Transient
	private User user;

	public String getCity() {
		return city;
	}

	public User getUser() {
		return user;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Address: " + city + " " + (user == null ? null : user.getId());
	}

}
